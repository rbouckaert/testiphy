package testiphy.beast2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import beast.core.BEASTInterface;
import beast.core.BEASTObject;
import beast.evolution.alignment.Alignment;
import beast.evolution.alignment.Sequence;
import beast.evolution.datatype.DataType;
import beast.evolution.likelihood.TreeLikelihood;
import beast.evolution.sitemodel.SiteModel;
import beast.evolution.substitutionmodel.Frequencies;
import beast.evolution.substitutionmodel.GTR;
import beast.evolution.substitutionmodel.HKY;
import beast.evolution.substitutionmodel.JukesCantor;
import beast.evolution.substitutionmodel.SubstitutionModel;
import beast.evolution.substitutionmodel.TN93;
import beast.evolution.tree.Tree;
import beast.util.NexusParser;
import beast.util.TreeParser;

// recursively go through directories
// calculate likelihood specified in command.txt file 
public class BEAST2LikelihoodCheck {
	static File current;
	
//	private  void parseCommand(File file) throws IOException {
//        parseCommand(readFile(file));
//	}

	private  String readFile(File file) throws IOException {
        BufferedReader fin = new BufferedReader(new FileReader(file));
        String str = null;
        while (fin.ready()) {
            str = fin.readLine();
        }
        fin.close();
        return str;
	}

	private boolean check(String dataFile, String treeFile, String model) throws IOException {
		List<BEASTInterface> list = loadFile(new File(current, dataFile));
		Alignment alignment = (Alignment) list.get(0);
		
		Tree tree = new TreeParser(readFile(new File(current, treeFile)));
		if (tree.getNodeCount() == 1) {
			NexusParser p = new NexusParser();
			p.parseFile(new File(current, treeFile));
			tree = p.trees.get(0);
		}
		
		SubstitutionModel substModel = parseSubstModel(model);
		if (substModel == null) {
			System.err.println("Unrecognised model, giving up");
			return false;
		}
		
		int gammaCategoryCount = 1;
		double shape = 1;
		int i = model.indexOf("Rates.gamma");
		if (i > 0) {
			// TODO: parse strings of the form Rates.gamma[n=4,alpha=0.5]
			gammaCategoryCount = 4;
			shape = 0.5;
		}
		double pInv = 0;
		i = model.indexOf("p_inv");
		if (i > 0) {
			// parse strings of the form +inv[p_inv=0.1]
			i += 7;
			int j = model.indexOf(']', i);
			pInv = Double.parseDouble(model.substring(i, j));			
		}
		
		SiteModel siteModel = new SiteModel();
		siteModel.initByName("substModel", substModel, 
				"gammaCategoryCount", gammaCategoryCount,
				"shape", shape + "",
				"proportionInvariant", pInv + "");
		
		TreeLikelihood likelihood = new TreeLikelihood();
		likelihood.initByName("data", alignment, "tree", tree, "siteModel", siteModel);
		double logP = likelihood.calculateLogP();
		
		String s = readFile(new File(current, "likelihood"));
		double target = Double.parseDouble(s);
		
		if (Math.abs(logP - target) < 1e-4) {
			System.out.println("Success!!");
			return true;
		} else {
			System.out.println("Fail!!" + logP + " != " + target);
			return false;
		}
	}

	private  SubstitutionModel parseSubstModel(String str) {
		SubstitutionModel model = null;
		if (str.indexOf("jc69") >= 0) {
			return new JukesCantor();
		}
		if (str.indexOf("tn93") >= 0) {
			int i = str.indexOf("kappaPur=");
			int j = str.indexOf(",", i);
			String kappaPur = str.substring(i+9, j);
			i = str.indexOf("kappaPyr=");
			j = str.indexOf(",", i);
			String kappaPyr = str.substring(i+9, j);
			model = new TN93();
			Frequencies freqs = new Frequencies();
			String f = parseFreqs(str, "ACGT");
			freqs.initByName("frequencies", f);
			((BEASTObject)model).initByName("kappa1", kappaPur, "kappa2", kappaPyr, "frequencies", freqs);
			return model;
		}
		if (str.indexOf("hky85") >= 0) {
			int i = str.indexOf("kappa=");
			int j = str.indexOf(",", i);
			String kappa = str.substring(i+6, j);
			model = new HKY();
			Frequencies freqs = new Frequencies();
			String f = parseFreqs(str, "ACGT");
			freqs.initByName("frequencies", f);
			((BEASTObject)model).initByName("kappa", kappa, "frequencies", freqs);
			return model;
		}
		if (str.indexOf("smodel=gtr") >= 0) {
			model = new GTR();
			Frequencies freqs = new Frequencies();
			String f = parseFreqs(str, "ACGT");
			freqs.initByName("frequencies", f);
			((BEASTObject)model).initByName("frequencies", freqs,
					"rateAC", getPairValue(str, "AC"),
					"rateAG", getPairValue(str, "AG"),
					"rateAT", getPairValue(str, "AT"),
					"rateCG", getPairValue(str, "CG"),
					"rateCT", getPairValue(str, "CT"),
					"rateGT", getPairValue(str, "GT")
					);
			return model;
		}
		return null;
	}

	private  Object getPairValue(String str, String string) {
		int j = str.indexOf("Pair[\""+string+"\",");
		int k = str.indexOf("]", j);
		return str.substring(j + ("Pair[\""+string+"\"").length() + 1, k);
	}

	private  String parseFreqs(String str, String order) {
		String freqs = "";
		for (int i = 0; i < order.length(); i++) {
			char c = order.charAt(i);
			freqs += getPairValue(str, c+"") + " " ;
		}
		return freqs;
	}

//	private  void traverse(File file)  throws IOException {
//		if (file.isDirectory()) {
//			for (File f : file.listFiles()) {
//				current = file;
//				traverse(f);
//			}
//		} else if (file.getName().equals("command.txt")) {
//			parseCommand(file);
//		}
//	}

	
	
		 public List<BEASTInterface> loadFile(File file) {
			List<BEASTInterface> selectedBEASTObjects = new ArrayList<>();
		    	try {
		    		// grab alignment data
		        	Map<String, StringBuilder> seqMap = new HashMap<>();
		        	List<String> taxa = new ArrayList<>();
		        	String currentTaxon = null;
					BufferedReader fin = new BufferedReader(new FileReader(file));
			        String missing = "?";
			        String gap = "-";
			        int totalCount = 4;
			        String datatype = "nucleotide";
			        // According to http://en.wikipedia.org/wiki/FASTA_format lists file formats and their data content
					// .fna = nucleic acid
					// .ffn = nucleotide coding regions
					// .frn = non-coding RNA
					// .ffa = amino acid
		    		boolean mayBeAminoacid = !(file.getName().toLowerCase().endsWith(".fna") || file.getName().toLowerCase().endsWith(".ffn") || file.getName().toLowerCase().endsWith(".frn"));
		    		
					while (fin.ready()) {
						String line = fin.readLine();
						if (line.startsWith(";")) {
							// it is a comment, ignore
						} else 	if (line.startsWith(">")) {
							// it is a taxon
							currentTaxon = line.substring(1).trim();
							// only up to first space
							currentTaxon = currentTaxon.replaceAll("\\s.*$", "");
						} else {
							// it is a data line
							if (currentTaxon == null) {
								fin.close();
								throw new RuntimeException("Expected taxon defined on first line");
							}
							if (seqMap.containsKey(currentTaxon)) {
								StringBuilder sb = seqMap.get(currentTaxon);
								sb.append(line);
							} else {
								StringBuilder sb = new StringBuilder();
								seqMap.put(currentTaxon, sb);
								sb.append(line);
								taxa.add(currentTaxon);
							}
						}
					}
					fin.close();
					
					int charCount = -1;
					Alignment alignment = new Alignment();
			        for (final String taxon : taxa) {
			            final StringBuilder bsData = seqMap.get(taxon);
			            String data = bsData.toString();
			            data = data.replaceAll("\\s", "");
			            seqMap.put(taxon, new StringBuilder(data));

			            if (charCount < 0) {charCount = data.length();}
			            if (data.length() != charCount) {
			                throw new IllegalArgumentException("Expected sequence of length " + charCount + " instead of " + data.length() + " for taxon " + taxon);
			            }
			            // map to standard missing and gap chars
			            data = data.replace(missing.charAt(0), DataType.MISSING_CHAR);
			            data = data.replace(gap.charAt(0), DataType.GAP_CHAR);

			            if (mayBeAminoacid && datatype.equals("nucleotide") && 
			            		guessSequenceType(data).equals("aminoacid")) {
			            	datatype = "aminoacid";
			            	totalCount = 20;
			            	for (Sequence seq : alignment.sequenceInput.get()) {
			            		seq.totalCountInput.setValue(totalCount, seq);
			            	}
			            }
			            
			            final Sequence sequence = new Sequence();
			            data = data.replaceAll("[Xx]", "?");
			            sequence.init(totalCount, taxon, data);
			            sequence.setID(NexusParser.generateSequenceID(taxon));
			            alignment.sequenceInput.setValue(sequence, alignment);
			        }
			        String ID = file.getName();
			        ID = ID.substring(0, ID.lastIndexOf('.')).replaceAll("\\..*", "");
			        alignment.setID(ID);


					alignment.dataTypeInput.setValue(datatype, alignment);
			        alignment.initAndValidate();
			        selectedBEASTObjects.add(alignment);
		    	} catch (Exception e) {
					e.printStackTrace();
					System.err.println( "Loading of " + file.getName() + " failed: " + e.getMessage());
		    	}
			return selectedBEASTObjects;
		}
		
	    /** Ported from jebl2
	     * Guess type of sequence from contents.
	     * @param seq the sequence
	     * @return SequenceType.NUCLEOTIDE or SequenceType.AMINO_ACID, if sequence is believed to be of that type.
	     *         If the sequence contains characters that are valid for neither of these two sequence
	     *         types, then this method returns null.
	     */
	     public String guessSequenceType(final String seq) {

	        int canonicalNucStates = 0;
	        int undeterminedStates = 0;
	        // true length, excluding any gaps
	        int sequenceLength = seq.length();
	        final int seqLen = sequenceLength;

	        boolean onlyValidNucleotides = true;
	        boolean onlyValidAminoAcids = true;

	        // do not use toCharArray: it allocates an array size of sequence
	        for(int k = 0; (k < seqLen) && (onlyValidNucleotides || onlyValidAminoAcids); ++k) {
	            final char c = seq.charAt(k);
	            final boolean isNucState = ("ACGTUXNacgtuxn?_-".indexOf(c) > -1);
	            final boolean isAminoState = true;

	            onlyValidNucleotides &= isNucState;
	            onlyValidAminoAcids &= isAminoState;

	            if (onlyValidNucleotides) {
	                assert(isNucState);
	                if (("ACGTacgt".indexOf(c) > -1)) {
	                    ++canonicalNucStates;
	                } else {
	                    if (("?_-".indexOf(c) > -1)) {
	                        --sequenceLength;
	                    } else if( ("UXNuxn".indexOf(c) > -1)) {
	                        ++undeterminedStates;
	                    }
	                }
	            }
	        }

	        String result = "aminoacid";
	        if (onlyValidNucleotides) {  // only nucleotide states
	            // All sites are nucleotides (actual or ambigoues). If longer than 100 sites, declare it a nuc
	            if( sequenceLength >= 100 ) {
	                result = "nucleotide";
	            } else {
	                // if short, ask for 70% of ACGT or N
	                final double threshold = 0.7;
	                final int nucStates = canonicalNucStates + undeterminedStates;
	                // note: This implicitely assumes that every valid nucleotide
	                // symbol is also a valid amino acid. This is true since we
	                // added support for the 21st amino acid, U (Selenocysteine)
	                // in AminoAcids.java.
	                result = nucStates >= sequenceLength * threshold ? "nucleotide" : "aminoacid";
	            }
	        } else if (onlyValidAminoAcids) {
	            result = "aminoacid";
	        } else {
	            result = null;
	        }
	        return result;
	    }

		
	static String [] testcases = new String[] {

			// unknown model
//			"tests/likelihood/lg/1 enolase-38-fsa.fasta enolase-38.tree lg08+f[pi=Frequencies.uniform]+Rates.gamma[n=4,alpha=0.5]+inv[p_inv=0.1]",
//			"tests/likelihood/codons/AAA/1 AAA-AAA.fasta AA.tree gy94[kappa=1,omega=1,pi=f1x4[pi=List[Pair[\"A\",0.25],Pair[\"C\",0.25],Pair[\"T\",0.25],Pair[\"G\",0.25]]]]",
//			"tests/likelihood/codons/AAA/2 AAA-ATA.fasta AA.tree gy94[kappa=1,omega=1,pi=f1x4[pi=List[Pair[\"A\",0.25],Pair[\"C\",0.25],Pair[\"T\",0.25],Pair[\"G\",0.25]]]]",
//			"tests/likelihood/codons/F1x4 P1au_TRUE_1.fas tree1a.tree gy94[kappa=2,omega=2,pi=f1x4[List[Pair[\"A\",0.1],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.4]]]]",
//			"tests/likelihood/codons/F3x4/1 P1au_TRUE_1.fas tree1a.tree gy94[kappa=2,omega=2,pi=f3x4[pi1=List[Pair[\"A\",0.1],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.4]],pi2=List[Pair[\"A\",0.1],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.4]],pi3=List[Pair[\"A\",0.1],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.4]]]]",
//			"tests/likelihood/codons/F3x4/2 P1au_TRUE_1.fas tree1a.tree gy94[kappa=2,omega=2,pi=f3x4[pi1=List[Pair[\"A\",0.1],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.4]],pi2=List[Pair[\"A\",0.4],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.1]],pi3=List[Pair[\"A\",0.3],Pair[\"C\",0.2],Pair[\"T\",0.1],Pair[\"G\",0.4]]]]",
//			"tests/likelihood/codons/M P1au_TRUE_1.fas tree1a.tree function[w,gy94[kappa=2,omega=w,pi=f1x4[pi=List[Pair[\"A\",0.1],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.4]]]]]+m3[,ps=List[0.3,0.3,0.4],omegas=List[0.0,0.3,1.0]]",
//			"tests/likelihood/codons/MG94 P1au_TRUE_1.fas tree1a.tree mg94[kappa=2,omega=2,pi=List[Pair[\"A\",0.1],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.4]]]",
//			"tests/likelihood/codons/MG94w9/1 P1au_TRUE_1.fas tree1a.tree mg94w9[kappa=2,omega=2,pi1=List[Pair[\"A\",0.1],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.4]],pi2=List[Pair[\"A\",0.1],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.4]],pi3=List[Pair[\"A\",0.1],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.4]]]",
//			"tests/likelihood/codons/MG94w9/2 P1au_TRUE_1.fas tree1a.tree mg94w9[kappa=2,omega=2,pi1=List[Pair[\"A\",0.1],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.4]],pi2=List[Pair[\"A\",0.4],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.1]],pi3=List[Pair[\"A\",0.3],Pair[\"C\",0.2],Pair[\"T\",0.1],Pair[\"G\",0.4]]]",
//			"tests/likelihood/codons/branch-site/1 P1au_TRUE_1.fas tree1a-fg.tree function[w,gy94[kappa=2,omega=w,pi=f1x4[List[Pair[\"A\",0.25],Pair[\"C\",0.25],Pair[\"T\",0.25],Pair[\"G\",0.25]]]]]+branch-site[,List[0.5,0.5],List[0.5],0.1,4,1]",
//			"tests/likelihood/codons/branch-site/2 P1au_TRUE_1.fas tree1a-fg.tree function[w,gy94[kappa=2,omega=w,pi=f1x4[pi=List[Pair[\"A\",0.25],Pair[\"C\",0.25],Pair[\"T\",0.25],Pair[\"G\",0.25]]]]]+branch-site[,fs=List[0.5,0.5],omegas=List[0.5],posP=0.1,posW=4,posSelection=0]"	
//			"tests/likelihood/triplets/MG94w9/1 P1au_TRUE_1.fas tree1a.tree let[q=hky85[2,List[Pair[\"A\",0.1],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.4]],getNucleotides],q3[q,q,q]]",
//			"tests/likelihood/triplets/MG94w9/2 P1au_TRUE_1.fas tree1a.tree let[pi1=List[Pair[\"A\",0.1],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.4]],let[pi2=List[Pair[\"A\",0.4],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.1]],let[pi3=List[Pair[\"A\",0.3],Pair[\"C\",0.2],Pair[\"T\",0.1],Pair[\"G\",0.4]],q3[hky85[2,pi1],hky85[2,pi2],hky85[2,pi3]]]]]",
			
			// wrong model? what is "+x3+" ?
//			"tests/likelihood/gtr_expected_gamma_inv/1 primates.fasta primates.newick --model=hky85[kappa=1,pi=List[Pair[\"A\",0.1],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.4]]]+Rates.gamma[4,0.5]+inv[p_inv=0.1]",
//			"tests/likelihood/gtr_expected_gamma_inv/2 primates.fasta primates.newick --model=hky85[kappa=1,pi=List[Pair[\"A\",0.1],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.4]]]+Rates.gamma[4,0.5]+inv[p_inv=0.9]",
//			"tests/likelihood/triplets/F1x4 P1au_TRUE_1.fas tree1a.tree hky85_sym[kappa=2,]+x3+f[pi=f1x4[List[Pair[\"A\",0.1],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.4]]]]",
//			"tests/likelihood/triplets/F3x4/1 P1au_TRUE_1.fas tree1a.tree hky85_sym[kappa=2,]+x3+f[pi=f3x4[pi1=List[Pair[\"A\",0.1],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.4]],pi2=List[Pair[\"A\",0.1],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.4]],pi3=List[Pair[\"A\",0.1],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.4]]]]",
//			"tests/likelihood/triplets/F3x4/2 P1au_TRUE_1.fas tree1a.tree hky85_sym[kappa=2,]+x3+f[f3x4[pi1=List[Pair[\"A\",0.1],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.4]],pi2=List[Pair[\"A\",0.4],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.1]],pi3=List[Pair[\"A\",0.3],Pair[\"C\",0.2],Pair[\"T\",0.1],Pair[\"G\",0.4]]]]",

			// these tests pass
			"tests/likelihood/1 5d.fasta 1.tree hky85[kappa=2,pi=List[Pair[\"A\",0.1],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.4]]]",
			"tests/likelihood/2 5d-muscle.fasta 1.tree hky85[kappa=2,pi=List[Pair[\"A\",0.1],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.4]]]",
			"tests/likelihood/3 5d-muscle.fasta 1.tree gtr[List[Pair[\"AC\",1],Pair[\"AG\",2],Pair[\"AT\",3],Pair[\"CG\",4],Pair[\"CT\",5],Pair[\"GT\",6]],pi=List[Pair[\"A\",0.1],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.4]]]",
			"tests/likelihood/4 5d-muscle.fasta 1.tree tn93[kappaPur=1.5,kappaPyr=2.5,pi=List[Pair[\"A\",0.1],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.4]]]",
			"tests/likelihood/jc_gamma/1 primates.fasta primates.tree jc69+Rates.gamma[4,0.5]",
			"tests/likelihood/gtr_gamma_inv/1 primates.fasta primates.tree hky85[kappa=2,pi=List[Pair[\"A\",0.1],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.4]]]+Rates.gamma[n=4,alpha=0.5]+inv[p_inv=0.1]",
			"tests/likelihood/gtr_gamma_inv/2 primates.fasta primates.tree hky85[kappa=2,pi=List[Pair[\"A\",0.1],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.4]]]+Rates.gamma[n=4,alpha=0.5]+inv[p_inv=0.9]",
			"tests/likelihood/gtr_inv/1 primates.fasta primates.tree hky85[kappa=2,pi=List[Pair[\"A\",0.1],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.4]]]+inv[p_inv=0.1]",
			"tests/likelihood/gtr_inv/2 primates.fasta primates.tree hky85[kappa=2,pi=List[Pair[\"A\",0.1],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.4]]]+inv[p_inv=0.9]",
			"tests/likelihood/triplets/MG94 P1au_TRUE_1.fas tree1a.tree let[q=hky85[kappa=2,pi=List[Pair[\"A\",0.1],Pair[\"C\",0.2],Pair[\"T\",0.3],Pair[\"G\",0.4]],a=getNucleotides],q3[q,q,q]]",

			// non-binary tree with internal node -- made to pass by using binary tree instead of original tree
			"tests/likelihood/two-sequences/1 two.fasta ../../../../data/12bin.tree -smodel=jc69",
			"tests/likelihood/theoretical/JC/1 AA.fasta ../../../../../data/AAbin.tree jc69",
			"tests/likelihood/theoretical/JC/2 AT.fasta ../../../../../data/AAbin.tree jc69",
			"tests/likelihood/theoretical/JC/AAA/1 AAA-AAA.fasta ../../../../../../data/AAbin.tree jc69",
			"tests/likelihood/theoretical/JC/AAA/2 AAA-ATA.fasta ../../../../../../data/AAbin.tree jc69",
			"tests/likelihood/theoretical/JC_INV/1 AA.fasta ../../../../../data/AAbin.tree jc69+inv[p_inv=0.1]",
			"tests/likelihood/theoretical/JC_INV/2 AA.fasta ../../../../../data/AAbin.tree jc69+inv[p_inv=0.9]",
			"tests/likelihood/theoretical/JC_INV/3 AT.fasta ../../../../../data/AAbin.tree jc69+inv[p_inv=0.1]",
			"tests/likelihood/theoretical/JC_INV/4 AT.fasta ../../../../../data/AAbin.tree jc69+inv[p_inv=0.9]",
			"tests/likelihood/triplets/AAA/1 AAA-AAA.fasta ../../../../../data/AAbin.tree hky85_sym[kappa=1,]+x3+f[f1x4[List[Pair[\"A\",0.25],Pair[\"C\",0.25],Pair[\"T\",0.25],Pair[\"G\",0.25]]]]",
			"tests/likelihood/triplets/AAA/2 AAA-ATA.fasta ../../../../../data/AAbin.tree hky85_sym[kappa=1,]+x3+f[f1x4[pi=List[Pair[\"A\",0.25],Pair[\"C\",0.25],Pair[\"T\",0.25],Pair[\"G\",0.25]]]]",
			
	};
	
	public static void main(String[] args) throws IOException {
		int total = testcases.length - 1;
		int success = 0;
		for (String str : testcases) {
			String [] testcase = str.split("\\s+");
			System.err.println("Processing " + testcase[0]);
			try {
				current = new File(testcase[0]);
				BEAST2LikelihoodCheck c = new BEAST2LikelihoodCheck();				
				if (c.check(testcase[1], testcase[2], testcase[3])) {
					success++;
				}
			} catch (RuntimeException e) {
				e.printStackTrace();
				System.err.println("while processing " + current.getAbsolutePath());
			}
		}
		System.err.println(success + " passed out of " + total);
	}

}
