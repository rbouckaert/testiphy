Introduction
------------

Testiphy is a cross-project likelihood testsuite for phylogenetic estimation
programs.  Testiphy provides standard data sets for comparing
likelihoods across programs. Testiphy aims to help software developers
* correctly implement complex substitution models
* implement CI testing
* provide likelihood unit tests for a variety of programs

Each unit test consists of an aligned data set, a tree, and a set of
parameter values.

Testiphy does not currently test the ability of programs to maximize
the likelihood, but tests the likelihood of specific parameter values.

Contributions of new tests, new code, and drivers for new programs are welcome!

Programs
--------

Testiphy currently has some coverage for BAli-Phy, RevBayes, PAUP, raxml-ng, IQ-TREE, and PhyML.

The goal is to add tests for PAML, BEAST1, BEAST2, phylobayes, and
other packages that compute phylogenetic likelihoods.

Test coverage
-------------

Testiphy currently has tests for
* GTR+G4+I
* the LG amino-acid model.
* codon models
  * Goldman-Yang '94
  * Muse-Gaut '94
  * M3
  * F1x4 and F3x4
  * branch-site

A few tests have theoretical results.

Integrating into CI
-------------------

1. Method #1 is to
  * add `testiphy` as a git submodule
  * run testiphy from automake, cmake, or meson as a test.

2. Method 2:
  * clone `testiphy` in `.travis-ci`.
  * install the software being tested into the $PATH
  * run testiphy on the software

Install
-------

```
git clone git@gitlab.com:testiphy/testiphy.git
cd testiphy
```

BAli-Phy
-------

If `bali-phy` is installed, then do:

`./testiphy bali-phy`

RevBayes
-------

If revbayes is installed, then do

`./testiphy rb`

For revbayes you should add the line

`outputPrecision=15`

to the file `~/.RevBayes.ini`.

PAUP*
-------

If paup is installed, then do

`./testiphy paup`

PhyML
-------

If phyml is installed, then do

`./testiphy phyml`

The binary is assumed to be called `phyml`.

IQ-TREE
-------

If iqtree is installed, then do

`./testiphy iqtree`

The binary is assumed to be called `iqtree`.

raxml-ng
-------

If raxml-ng is installed, then do

`./testiphy raxml-ng`

HyPhy
-------

If hyphy is installed, then do

`./testiphy hyphymp`

The binary is assumed to be called `hyphymp`.

